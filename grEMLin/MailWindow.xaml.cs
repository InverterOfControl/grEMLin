﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace grEMLin
{
    /// <summary>
    /// Interaction logic for MailWindow.xaml
    /// </summary>
    public partial class MailWindow : Window
    {
        public MailWindow(CDO.Message msg)
        {
            InitializeComponent();

            bool isHtml = string.IsNullOrWhiteSpace(msg.TextBody);

            if (isHtml)
            {
                webBrowser.NavigateToString(msg.HTMLBody);
            }
            else
            {
                webBrowser.NavigateToString(msg.TextBody);
            }

            subjectTxtBox.Text = msg.Subject;
            fromTxtBox.Text = msg.From;
            toTxtBox.Text = msg.To;
            dateTxtBox.Text = msg.SentOn.ToString("dd.MM.yyyy HH:mm");

            this.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
