﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace grEMLin
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public CDO.Message Message { get; private set; }

        internal void DisplayError(string errorString)
        {
            MessageBox.Show(errorString);
        }

        internal void InitializeComponent(CDO.Message msg)
        {
            this.Message = msg;
        }
    }
}
