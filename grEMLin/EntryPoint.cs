﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace grEMLin
{
    public class EntryPoint
    {
        [STAThread]
        public static void Main(string[] args)
        {
            string errorString = string.Empty;

            if (args == null || args.Length == 0)
            {
                errorString = "No file passed as argument!";
                DisplayError(errorString);
                return;
            }
           
            // args[0] should be the eml file
            string emlFilePath = args[0];

            if (!File.Exists(emlFilePath))
            {
                errorString = "File does not exist!";
                DisplayError(errorString);
                return;
            }

            CDO.Message msg = ReadMessage(emlFilePath);

            ExecuteApp(msg);
        }


        private static void DisplayError(string errorString){
            var app = new App();
            
            if (!string.IsNullOrWhiteSpace(errorString))
            {
                app.DisplayError(errorString);
            }

            app.Shutdown();
        }

        private static void ExecuteApp(CDO.Message msg)
        {
            MailWindow mw = new MailWindow(msg);
            
            var app = new App();
            app.InitializeComponent(msg);
            app.Run();
        }


        protected static CDO.Message ReadMessage(String emlFileName)
        {
            CDO.Message msg = new CDO.Message();
            ADODB.Stream stream = new ADODB.Stream();
            stream.Open(Type.Missing, ADODB.ConnectModeEnum.adModeUnknown, ADODB.StreamOpenOptionsEnum.adOpenStreamUnspecified, String.Empty, String.Empty);
            stream.LoadFromFile(emlFileName);
            stream.Flush();
            msg.DataSource.OpenObject(stream, "_Stream");
            msg.DataSource.Save();
            return msg;
        }
    }
}
